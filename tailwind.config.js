module.exports = {
  mode: "jit",
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens: {},
      colors: {
        background: "#FFF5ED",
        darkred: "#9D1A16",
        darkgray: "#2D3143",
        lightgray: "#F8F8F8",
        orange: "#F5BE73",
        darkorange: "#DBA457",
        blue: "#A0C6F2",
        darkblue: "#6DA6E8",
        green: "#ACE0AF",
        darkgreen: "#86CB8A",
        pink: "#DDBBBB",
        hoverhighlight: "#F5EAE1",
        eventred: "#F78D85",
        initialred: "#E26161",
      },
      fontFamily: {
        poppins: ["Poppins", "HK Grotesk", "sans-serif"],
        grotesk: ["HK Grotesk", "Poppins", "sans-serif"],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
