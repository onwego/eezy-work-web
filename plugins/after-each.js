// plugins/after-each.js;
export default function ({ app }) {
    app.router.afterEach((to, from, next) => {
        // Do something
        if(!sessionStorage.getItem("accessToken")){
            if(to.path !== '/login'){ 
                // console.log(to)
                // console.log(from)
                app.router.push('/login');
            }
        }
    });
}

// // plugins/after-each.js;
// export default async ({ app }) => {
//     app.router.beforeEach((to, from, next) => {
//         // Do something
//         if(!sessionStorage.getItem("accessToken")){
//             if(to.path != '/login'){ 
//                 // console.log(to)
//                 // console.log(from)
//                 next('/login');
//             }
//         }
//     });
// }