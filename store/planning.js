import apiClient from '@/utils/api';
import axios from 'axios';

export const state = () => ({
  browserVisible: true,  // Or some default value
  allEvents: [],
  latestRenewedDate: '',
  latestRenewedDateTime: '',
  disableRefresh: false,
  renewAvailable: true,
  loading: false,
  error: null,
  companyId: null,
});

export const mutations = {
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
  SET_EVENTS(state, events) {
    state.allEvents = events;
  },
  SET_LATEST_RENEWED_DATE(state, date) {
    state.latestRenewedDate = date;
  },
  SET_LATEST_RENEWED_DATETIME(state, datetime) {
    state.latestRenewedDateTime = datetime;
  },
  SET_DISABLE_REFRESH(state, status) {
    state.disableRefresh = status;
  },
  SET_RENEW_AVAILABLE(state, status) {
    state.renewAvailable = status;
  },
};

export const actions = {
  fetchEvents({ commit, state }, { theEventUrl }) {
    return new Promise((resolve, reject) => {
      if (state.browserVisible) {
        commit('SET_DISABLE_REFRESH', true);
        const today = new Date();
        const year = today.getFullYear();
        const month = String(today.getMonth() + 1).padStart(2, "0");
        const day = String(today.getDate()).padStart(2, "0");
        const hours = String(today.getHours()).padStart(2, "0");
        const minutes = String(today.getMinutes()).padStart(2, "0");
        const seconds = String(today.getSeconds()).padStart(2, "0");

        const latestRenewedDate = `${hours}:${minutes}:${seconds}`;
        commit('SET_LATEST_RENEWED_DATE', latestRenewedDate);

        const datetime = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
        commit('SET_LATEST_RENEWED_DATETIME', datetime);

        const eventUrl = theEventUrl;
        const filters = {};

        console.log('eventUrl');
        console.log(eventUrl);

        axios.post(eventUrl, filters)
          .then((response) => {
            commit('SET_EVENTS', response.data);
            commit('SET_DISABLE_REFRESH', false);
            commit('SET_RENEW_AVAILABLE', false);
            resolve(response.data);
          })
          .catch((error) => {
            console.error(error);
            commit('SET_DISABLE_REFRESH', false);
            reject(error);
          });
      } else {
        resolve([]);
      }
    });
  },
};

export const getters = {
  allEvents: state => state.allEvents,
  latestRenewedDate: state => state.latestRenewedDate,
  latestRenewedDateTime: state => state.latestRenewedDateTime,
  disableRefresh: state => state.disableRefresh,
  renewAvailable: state => state.renewAvailable,
  loading: state => state.loading,
  error: state => state.error,
};
