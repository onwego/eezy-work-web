import apiClient from '@/utils/api';

export const state = () => ({
  hours: [],
  loading: false,
  error: null,
  companyId: null,
});

export const mutations = {
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_SERVICES(state, services) {
    state.services = services;
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
};

export const actions = {
  async fetchHours({ commit }) {
    commit('SET_LOADING', true);
    try {
      const response = await apiClient.get('/hours');
      commit('SET_HOURS', response.data);
    } catch (error) {
      commit('SET_ERROR', error);
    } finally {
      commit('SET_LOADING', false);
    }
  },
};

export const getters = {
  getServices: (state) => state.services,
  isLoading: (state) => state.loading,
  getError: (state) => state.error,
};
