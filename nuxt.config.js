import trailingSlashRedirect from "./middleware/trailingSlashRedirect";

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: "EEZY Work",
    title: "EEZY Work",
    htmlAttrs: {
      lang: "nl",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
    script: [
      {
        src: "https://maps.googleapis.com/maps/api/js?key=AIzaSyDpaoViOg3jb8ZYx3baSurmmaBoKUpekLU&libraries=places",
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["@/static/css/vue-multiselect.css"],
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    // { src: '~/plugins/after-each.js', mode: 'client' },
    { src: "~/plugins", ssr: false },
    { src: "~/plugins/pop-over" },
    { src: "~/plugins/vue-json-excel" },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    "@nuxtjs/tailwindcss",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/axios", "@nuxtjs/auth-next"],
  auth: {
    localStorage: false,
    redirect: {
      home: '/login/profielen?fl=1'
    },
    // watchLoggedIn: true,
    strategies: {
      local: {
        token: {
          property: "token",
          global: true,
        },
        user: {
          property: "user",
          autoFetch: false,
        },
        endpoints: {
          login: {
            url: "https://api.eezywork.nl/v1/login-new.php",
            method: "post",
          },
          logout: false,
          user: false,
        },
      },
    },
  },
  router: {
    middleware: ["auth-check", "trailingSlashRedirect"],
    props: true,
    history: true,
    trailingSlash: false,
  },

  build: {
    publicPath: "https://app.eezywork.nl/",
    filenames: { chunk: () => "[name].js" },
    // terser: {
    // // https://github.com/terser/terser#compress-options
    // terserOptions: {
    //   compress: {
    //     drop_console: true
    //   }
    // }
  },
  generate: {
    subFolders: true,
    fallback: "404.html",
  },
  ssr: false,
  loading: false,
  loadingIndicator: false,
  server: {
    port: 3000,
    host: "localhost",
  },
};
