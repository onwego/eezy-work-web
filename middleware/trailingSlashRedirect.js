export default function ({ route, redirect }) {
  // If the route path ends with a slash. Delete that slash.
    if (route.path !== '/' && route.path.endsWith('/')) {
      const { path, query, hash } = route;
      const nextPath = path.replace(/\/+$/, '') || '/';
      const nextRoute = { path: nextPath, query, hash };
      redirect(nextRoute);
    }
  }