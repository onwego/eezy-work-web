export default function ({ route, redirect, $auth }) {
  const axios = require("axios");
  // If user is not loggedin or UID does not exist. return to login.
  if (!$auth.loggedIn || !$auth.user.uid) {
    // Except these routes:
    if (
      route.path == "/login/wachtwoord" ||
      route.path == "/login/wachtwoord/" ||
      route.path == "/login/wachtwoord/aanvragen/" ||
      route.path == "/login/wachtwoord/aanvragen" ||
      route.path == "/login/verifieer" ||
      route.path == "/login/verifieer/" ||
      route.path == "/login/uitnodiging/" ||
      route.path == "/login/uitnodiging"
    ) {
    } else if (route.path != "/login") {
      $auth.logout();
      redirect("/login");
    }
  }

  // If on login page when logged in. Return to home page.
  if ($auth.loggedIn && route.path == "/login") {
    redirect("/login/profielen");
  }
  // If logged in. Check AUTH Permissions.
  if ($auth.loggedIn) {
    const profileId = $auth.user.profileId;
    var url =
      "https://api.eezywork.nl/v1/companies/profile/get-permissions.php";
    axios
      .post(url, {
        profileId: profileId,
      })
      .then((response) => {
        if (response.data.length > 0) {
          $auth.$storage.setState("perm", response.data);
        } else {
          $auth.$storage.setState("perm", [{ id: 0, name: "null" }]);
        }
      })
      .catch((error) => {
        $auth.$storage.setState("perm", [{ id: 0, name: "null" }]);
      });
  }
  const ua = navigator;

  var hardwareConcurrency = '';
  var deviceMemory = '';
  var userAgent = '';

  if(navigator){
    if(navigator.hardwareConcurrency){
      hardwareConcurrency = navigator.hardwareConcurrency.toString();
    }
    if(navigator.deviceMemory){
      deviceMemory = navigator.deviceMemory.toString();
    }
    if(navigator.userAgent){
      userAgent = navigator.userAgent;
    }
  }

  var userDeviceID = hardwareConcurrency + deviceMemory + userAgent;
  if ($auth.loggedIn) {
    //Check if IP is the same
    var resourcesUrl = "https://api.ipify.org?format=json";
    axios.get(resourcesUrl)
    .then(response => {
    var url = "https://api.eezywork.nl/v1/auth/check-ip.php";
    axios.post(url, {
        ip: userDeviceID,
        id: $auth.user.id
    }).then((response) => {
        if(!response.data.exists){
            $auth.logout();
            redirect('/login');
        }
    })
    }).catch(error => {
        if(!response.data.exists){
            $auth.logout();
            redirect('/login');
        }
    });
  }
}
