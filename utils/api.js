import axios from 'axios';

const BASE_URL = 'https://api.eezywork.nl';
const VERSION = '/v1';
const API_URL = BASE_URL + VERSION;

const apiClient = axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

export default apiClient;